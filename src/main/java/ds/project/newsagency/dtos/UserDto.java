package ds.project.newsagency.dtos;

import ds.project.newsagency.entities.Category;

import java.util.List;
import java.util.Objects;

public class UserDto {

    private Long id;
    private String username;
    private String password;
    private String email;
    private boolean admin;
    private List<Long> subscriptionIds;

    public UserDto(){}
    public UserDto(Long id, String username, String password, String email, boolean admin, List<Long> subscriptionIds) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.admin = admin;
        this.subscriptionIds = subscriptionIds;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public List<Long> getSubscriptionIds() {
        return subscriptionIds;
    }

    public void setSubscriptionIds(List<Long> subscriptionIds) {
        this.subscriptionIds = subscriptionIds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDto userDto = (UserDto) o;
        return admin == userDto.admin &&
                Objects.equals(id, userDto.id) &&
                Objects.equals(username, userDto.username) &&
                Objects.equals(password, userDto.password) &&
                Objects.equals(email, userDto.email) &&
                Objects.equals(subscriptionIds, userDto.subscriptionIds);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, password, email, admin, subscriptionIds);
    }
}
