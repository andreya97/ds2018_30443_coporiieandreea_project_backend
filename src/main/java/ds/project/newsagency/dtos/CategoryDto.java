package ds.project.newsagency.dtos;

import ds.project.newsagency.entities.Article;
import ds.project.newsagency.entities.User;

import java.util.List;
import java.util.Objects;

public class CategoryDto {
    private Long id;
    private String name;
    private String description;
    private List<Long> articleIds;
    private List<Long> subscriberIds;

    public CategoryDto(){}
    public CategoryDto(Long id, String name, String description, List<Long> articleIds, List<Long> subscriberIds) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.articleIds = articleIds;
        this.subscriberIds = subscriberIds;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Long> getArticleIds() {
        return articleIds;
    }

    public void setArticleIds(List<Long> articleIds) {
        this.articleIds = articleIds;
    }

    public List<Long> getSubscriberIds() {
        return subscriberIds;
    }

    public void setSubscriberIds(List<Long> subscriberIds) {
        this.subscriberIds = subscriberIds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryDto that = (CategoryDto) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(articleIds, that.articleIds) &&
                Objects.equals(subscriberIds, that.subscriberIds);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, articleIds, subscriberIds);
    }
}
