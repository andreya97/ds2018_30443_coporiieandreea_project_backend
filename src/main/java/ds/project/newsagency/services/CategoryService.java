package ds.project.newsagency.services;

import ds.project.newsagency.dtos.ArticleDto;
import ds.project.newsagency.dtos.CategoryDto;
import ds.project.newsagency.entities.Category;
import ds.project.newsagency.entities.User;
import ds.project.newsagency.exceptions.GenericException;
import ds.project.newsagency.mappers.CategoryMapper;
import ds.project.newsagency.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CategoryService {

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    EmailService emailService;

    @Autowired
    UserService userService;
    @Autowired
    ArticleService articleService;

    @Autowired
    CategoryMapper categoryMapper;

    public CategoryDto create(Category category) {
        if (categoryRepository.findByName(category.getName()) != null) {
            throw new GenericException(HttpStatus.FOUND, "Category with this name already exists");
        }
        return categoryMapper.toDto(categoryRepository.save(category));
    }

    public CategoryDto getById(Long id) {
        if (categoryRepository.findById(id).equals( Optional.empty()) ) {
            throw new GenericException(HttpStatus.NOT_FOUND, "Category does not exist.");
        }
        return categoryMapper.toDto(categoryRepository.findById(id).get());
    }

    public List<CategoryDto> getAll() {
        return categoryMapper.toDtos(categoryRepository.findAll());
    }

    public CategoryDto update(Long id, CategoryDto categoryDto) {
        if (categoryRepository.findById(id).equals( Optional.empty()) ) {
            throw new GenericException(HttpStatus.NOT_FOUND, "Cannot find category to update.");
        }
        Category category = categoryMapper.toCategory(categoryDto);
        Category c = categoryRepository.findById(id).get();
         String oldName = c.getName();
        if (category.getName() != null) c.setName(category.getName());
        if (category.getDescription() != null) c.setDescription(category.getDescription());
         emailService.notifySubscribers(c,"Updated");
        return categoryMapper.toDto(categoryRepository.save(c));
    }

    public Long deleteById(Long id) {
        if (categoryRepository.findById(id).equals( Optional.empty()) ) {
            throw new GenericException(HttpStatus.NOT_FOUND, "Cannot find category to delete.");
        }
        List<ArticleDto> articles = articleService.findByCategoryId(id);
        if (articles != null && !articles.isEmpty())
        for (ArticleDto a : articleService.findByCategoryId(id)) {
            a.setCategoryId((long)-1);
            articleService.update(a.getId(), a);
        }

        if (articleService.findByCategoryId(id) == null ||  articleService.findByCategoryId(id).isEmpty()) {
            System.out.println("ok");
        }
        Category c = categoryRepository.findById(id).get();
        emailService.notifySubscribers(c,"Deleted");
        categoryRepository.deleteById(id);
        return id;
    }
}
