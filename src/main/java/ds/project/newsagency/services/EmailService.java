package ds.project.newsagency.services;

import ds.project.newsagency.entities.Article;
import ds.project.newsagency.entities.Category;
import ds.project.newsagency.entities.User;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Service
public class EmailService {
    private String username;
    private String password;
    private Properties props;

    public EmailService() {
        this.username = "mockingemailaddress@yahoo.com";
        this.password = "!abcd1234";

        props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.mail.yahoo.com");
        props.put("mail.smtp.port", "587");
    }


    public void sendMail(String to, String subject, String content) {
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));
            message.setSubject(subject);
            message.setText(content);

            Transport.send(message);

            System.out.println("Mail sent.");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public void notifySubscribers(Category c, String op) {
        if (c.getSubscribers().size() != 0)
        for (User s : c.getSubscribers()) {
            sendMail(s.getEmail(), "Change in your subscription on " + c.getName(), op);
        }
    }

    public void notifySubscribers(Article a, String op) {
//        if (a.getCategory().getSubscribers().size() != 0)
//        for (User s : a.getCategory().getSubscribers()) {
//            sendMail(s.getEmail(), "Change in the article " + a.getTitle() + " of category  " + a.getCategory(), op);
//        }
    }
}

