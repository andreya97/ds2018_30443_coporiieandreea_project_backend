package ds.project.newsagency.services;

import ds.project.newsagency.dtos.ArticleDto;
import ds.project.newsagency.dtos.CategoryDto;
import ds.project.newsagency.entities.Article;
import ds.project.newsagency.entities.Category;
import ds.project.newsagency.exceptions.GenericException;
import ds.project.newsagency.mappers.ArticleMapper;
import ds.project.newsagency.repositories.ArticleRepository;
import ds.project.newsagency.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Service
@Transactional
public class ArticleService {

    @Autowired
    ArticleRepository articleRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    CategoryService categoryService;

    @Autowired
     ArticleMapper articleMapper;

    @Autowired
    EmailService emailService;

    public ArticleDto create(ArticleDto articleDto) {
        if (articleRepository.findByTitle(articleDto.getTitle()) != null) {
            throw new GenericException(HttpStatus.FOUND, "Article with this title already exists");
        }
        Article article = articleMapper.toArticle(articleDto);
        return articleMapper.toDto(articleRepository.save(article));
    }

    public List<ArticleDto> getAll() {
        return articleMapper.toDtos(articleRepository.findAll());
    }

    public List<ArticleDto> getRelatedArticles(Long id) {
        ArticleDto a = getById(id);
        CategoryDto c = categoryService.getById(a.getCategoryId());
        c.getArticleIds().remove(a.getId());
        return articleMapper.toDtos(articleRepository.findAllByIdIn(c.getArticleIds()));
    }

    public ArticleDto getById(Long id) {
        if (articleRepository.findById(id).equals( Optional.empty()) ) {
            throw new GenericException(HttpStatus.NOT_FOUND, "Article does not exist.");
        }
        return articleMapper.toDto(articleRepository.findById(id).get());
    }

    public ArticleDto update(Long id, ArticleDto article) {
        if (articleRepository.findById(id).equals( Optional.empty()) ) {
            throw new GenericException(HttpStatus.NOT_FOUND, "Cannot find article to update.");
        }
        Article a = articleRepository.findById(id).get();
        if (article.getTitle() != null) a.setTitle(article.getTitle());
        if (article.getAuthor() != null) a.setAuthor(article.getAuthor());

        if (article.getCategoryId() != null) {
            if (article.getCategoryId() == -1) a.setCategory(null);
            else
                a.setCategory(categoryRepository.findById(article.getCategoryId()).get());
        }
        if (article.getBody() != null) a.setBody(article.getBody());

        emailService.notifySubscribers(a, "Updated");

        return articleMapper.toDto(articleRepository.save(a));
    }

    public List<ArticleDto> findByCategoryId(Long id) {
        return articleMapper.toDtos(articleRepository.findByCategory_Id(id));
    }

    public Long deleteById(Long id) {
        if (articleRepository.findById(id).equals( Optional.empty()) ) {
            throw new GenericException(HttpStatus.NOT_FOUND, "Cannot find article to delete.");
        }
        articleRepository.deleteById(id);
        emailService.notifySubscribers(articleMapper.toArticle(getById(id)), "Deleted");
        return id;
    }
}
