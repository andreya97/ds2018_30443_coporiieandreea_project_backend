package ds.project.newsagency.services;

import ds.project.newsagency.dtos.UserDto;
import ds.project.newsagency.entities.Category;
import ds.project.newsagency.entities.User;
import ds.project.newsagency.exceptions.GenericException;
import ds.project.newsagency.mappers.UserMapper;
import ds.project.newsagency.repositories.CategoryRepository;
import ds.project.newsagency.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    UserMapper userMapper;

    public UserDto create(User user) {
        if (userRepository.findByUsername(user.getUsername()) != null || userRepository.findByEmail(user.getEmail()) != null) {
            throw new GenericException(HttpStatus.FOUND, "User with this username/email already exists");
        }
        return userMapper.toDto(userRepository.save(user));
    }

    public UserDto getById(Long id) {
        if (userRepository.findById(id).equals( Optional.empty()) ) {
            throw new GenericException(HttpStatus.NOT_FOUND, "User does not exist.");
        }
        return userMapper.toDto(userRepository.findById(id).get());
    }

    public List<UserDto> getAll() {
        return userMapper.toDtos(userRepository.findAll());
    }

    public UserDto getByUserAndPass(String username, String password) {
        User u = userRepository.findByUsernameAndPassword(username,password);
        if (u == null){
            throw new GenericException(HttpStatus.NOT_FOUND, "Wrong username or password");
        }
        return userMapper.toDto(u);
    }

    public UserDto update(Long id, User user) {
        if (userRepository.findById(id).equals( Optional.empty()) ) {
            throw new GenericException(HttpStatus.NOT_FOUND, "Cannot find user to update.");
        }
        User u = userRepository.findById(id).get();
        if (user.getUsername() != null) u.setUsername(user.getUsername());
        if (user.getPassword() != null) u.setPassword(user.getPassword());
        if (user.getEmail() != null) u.setEmail(user.getEmail());
        if (user.getSubscriptions() != null) u.setSubscriptions(user.getSubscriptions());
        return userMapper.toDto(userRepository.save(u));
    }

    public Long deleteById(Long id) {
        if (userRepository.findById(id).equals( Optional.empty()) ) {
            throw new GenericException(HttpStatus.NOT_FOUND, "Cannot find user to delete.");
        }
        userRepository.deleteById(id);
        return id;
    }

    public void subscribe(Long userid, Long categoryid) {
        Category c = categoryRepository.findById(categoryid).orElse(null);
        User u = userRepository.findById(userid).orElse(null);
        if (u == null || c == null) {
            throw new GenericException(HttpStatus.NOT_FOUND,"User or cateogry not found.");
        }
        u.getSubscriptions().add(c);
        c.getSubscribers().add(u);
        userRepository.save(u);
    }

    public void unsubscribe(Long userid, Long categoryid) {
        Category c = categoryRepository.findById(categoryid).orElse(null);
        User u = userRepository.findById(userid).orElse(null);
        if (u == null || c == null) {
            throw new GenericException(HttpStatus.NOT_FOUND,"User or cateogry not found.");
        }
        u.getSubscriptions().remove(c);
        c.getSubscribers().remove(u);
        userRepository.save(u);
    }

    public boolean isSubscribed(User u, Category c) {
        return c.getSubscribers().contains(u);
    }
}
