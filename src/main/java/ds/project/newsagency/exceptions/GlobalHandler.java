package ds.project.newsagency.exceptions;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = { GenericException.class })
    protected ResponseEntity<Object> handleGenericEx(GenericException ex) {
        return new ResponseEntity<>(ex.getMessage(),ex.getStatus());
    }
}