package ds.project.newsagency.controllers;

import ds.project.newsagency.dtos.CategoryDto;
import ds.project.newsagency.entities.Category;
import ds.project.newsagency.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    @PostMapping("/create")
    public CategoryDto getCategory(@NotNull @RequestBody Category category) {
        return categoryService.create(category);
    }

    @GetMapping("/get/{id}")
    public CategoryDto getCategory(@NotNull @PathVariable Long id) {
        return categoryService.getById(id);
    }

    @GetMapping("/get/all")
    public List<CategoryDto> getCategory() {
        return categoryService.getAll();
    }

    @PostMapping("/update/{id}")
    public CategoryDto updateCategory(@NotNull @PathVariable Long id, @Valid @RequestBody CategoryDto category) {
        return categoryService.update(id, category);
    }

    @PostMapping("/delete/{id}")
    public Long deleteCategory(@NotNull @PathVariable Long id) {
        return categoryService.deleteById(id);
    }

}
