package ds.project.newsagency.controllers;

import ds.project.newsagency.dtos.LoginDto;
import ds.project.newsagency.dtos.UserDto;
import ds.project.newsagency.entities.User;
import ds.project.newsagency.exceptions.GenericException;
import ds.project.newsagency.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class RootController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/**", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<String> anyPath() {
        throw new GenericException(HttpStatus.BAD_REQUEST, "Unmapped request");
    }

    @RequestMapping(value="/login", method = RequestMethod.POST)
    public UserDto login(@NotNull @RequestBody LoginDto loginDto) {
        return userService.getByUserAndPass(loginDto.getUsername(), loginDto.getPassword());

    }

    @RequestMapping(value="/register", method = RequestMethod.POST)
    public UserDto registerUser(@Valid @RequestBody User user) {
        return userService.create(user);
    }
}
