package ds.project.newsagency.controllers;
import ds.project.newsagency.dtos.UserDto;
import ds.project.newsagency.entities.User;
import ds.project.newsagency.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;


    @GetMapping("/get/{id}")
    public UserDto getUser(@NotNull @PathVariable Long id) {
        return userService.getById(id);
    }

    @GetMapping("/get/all")
    public List<UserDto> getUsers() {
        return userService.getAll();
    }

    @PostMapping("/update/{id}")
    public UserDto updateUser(@NotNull @PathVariable Long id, @Valid @RequestBody User user) {
        return userService.update(id, user);
    }

    @PostMapping("/delete/{id}")
    public Long deleteUser(@NotNull @PathVariable Long id) {
        return userService.deleteById(id);
    }

    @PostMapping("/subscribe")
    public void subscribe(@NotNull @RequestParam Long userid,@NotNull @RequestParam Long categoryid) {
        userService.subscribe(userid,categoryid);
    }

    @PostMapping("/unsubscribe")
    public void unsubscribe(@NotNull @RequestParam Long userid,@NotNull @RequestParam Long categoryid) {
        userService.unsubscribe(userid,categoryid);
    }
}
