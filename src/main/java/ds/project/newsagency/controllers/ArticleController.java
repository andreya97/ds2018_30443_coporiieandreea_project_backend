package ds.project.newsagency.controllers;

import ds.project.newsagency.dtos.ArticleDto;
import ds.project.newsagency.entities.Article;
import ds.project.newsagency.services.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/article")
public class ArticleController {

    @Autowired
    ArticleService articleService;

    @PostMapping("/create")
    public ArticleDto getArticle(@NotNull @RequestBody ArticleDto article) {
        return articleService.create(article);
    }

    @GetMapping("/get/all")
    public List<ArticleDto> getArticles() {
        return articleService.getAll();
    }

    @GetMapping("/get/{id}")
    public ArticleDto getArticle(@NotNull @PathVariable Long id) {
        return articleService.getById(id);
    }

    @GetMapping("/get/related/{id}")
    public List<ArticleDto> getArticles(@NotNull @PathVariable Long id) {
        return articleService.getRelatedArticles(id);
    }

    @PostMapping("/update/{id}")
    public ArticleDto updateArticle(@NotNull @PathVariable Long id, @Valid @RequestBody ArticleDto article) {
        return articleService.update(id, article);
    }

    @PostMapping("/delete/{id}")
    public Long deleteArticle(@NotNull @PathVariable Long id) {
        return articleService.deleteById(id);
    }

}
