package ds.project.newsagency.mappers;

import ds.project.newsagency.dtos.ArticleDto;
import ds.project.newsagency.entities.Article;
import ds.project.newsagency.entities.Category;
import ds.project.newsagency.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ArticleMapper {

    @Autowired
    private CategoryRepository categoryRepository;

    public List<ArticleDto> toDtos(List<Article> articles) {
        return articles.stream().map(this::toDto).collect(Collectors.toList());
    }

    public List<Article> toArticles(List<ArticleDto> articleDtos) {
        return articleDtos.stream().map(this::toArticle).collect(Collectors.toList());
    }

    public ArticleDto toDto(Article article) {
       return new ArticleDto(article.getId(),article.getTitle(), article.getAuthor(),  article.getBody(),
               article.getCategory() != null ? article.getCategory().getId() : null);
    }

    public Article toArticle(ArticleDto articleDto) {
        Category c = categoryRepository.findById(articleDto.getCategoryId()).orElse(null);
        return new Article(articleDto.getTitle(),articleDto.getAuthor(),articleDto.getBody(),c);
    }
}
