package ds.project.newsagency.mappers;

import ds.project.newsagency.dtos.UserDto;
import ds.project.newsagency.entities.Category;
import ds.project.newsagency.entities.User;
import ds.project.newsagency.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserMapper {

    @Autowired
    private CategoryRepository categoryRepository;


    public List<UserDto> toDtos(List<User> users) {
        return users.stream().map(this::toDto).collect(Collectors.toList());
    }

    public List<User> toUsers(List<UserDto> userDtos) {
        return userDtos.stream().map(this::toUser).collect(Collectors.toList());
    }

    public UserDto toDto(User user) {
        List<Long> subscriptionIds = new ArrayList<>();
        if (user.getSubscriptions() != null)
            subscriptionIds = user.getSubscriptions().stream().map(Category::getId).collect(Collectors.toList());
        return new UserDto(user.getId(),user.getUsername(),user.getPassword(),user.getEmail(),user.isAdmin(),subscriptionIds);
    }

    public User toUser(UserDto userDto) {
        List<Category> subscriptions = new ArrayList<>();
        if (userDto.getSubscriptionIds() != null)
            subscriptions = userDto.getSubscriptionIds().stream().map(e -> categoryRepository.findById(e).orElse(null)).collect(Collectors.toList());
        return new User(userDto.getUsername(),userDto.getPassword(),userDto.getEmail(),userDto.isAdmin(),subscriptions);
    }
}
