package ds.project.newsagency.mappers;

import ds.project.newsagency.dtos.CategoryDto;
import ds.project.newsagency.entities.Article;
import ds.project.newsagency.entities.Category;
import ds.project.newsagency.entities.User;
import ds.project.newsagency.repositories.ArticleRepository;
import ds.project.newsagency.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CategoryMapper {

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private UserRepository userRepository;

    public List<CategoryDto> toDtos(List<Category> categories) {
        return categories.stream().map(this::toDto).collect(Collectors.toList());
    }

    public List<Category> toCategories(List<CategoryDto> categoryDtos) {
        return categoryDtos.stream().map(this::toCategory).collect(Collectors.toList());
    }

    public CategoryDto toDto(Category cateogry) {
        List<Long> articleIds = new ArrayList<>();
        List<Long> userIds = new ArrayList<>();
        if (cateogry.getArticles() != null)
             articleIds = cateogry.getArticles().stream().map(Article::getId).collect(Collectors.toList());
        if (cateogry.getSubscribers() != null)
             userIds = cateogry.getSubscribers().stream().map(User::getId).collect(Collectors.toList());
        return new CategoryDto(cateogry.getId(),cateogry.getName(),cateogry.getDescription(),articleIds,userIds);
    }

    public Category toCategory(CategoryDto categoryDto) {
        List<Article> articles = new ArrayList<>();
        List<User> subscribers = new ArrayList<>();
        if (categoryDto.getArticleIds() != null)
            articles = categoryDto.getArticleIds().stream().map(e -> articleRepository.findById(e).orElse(null)).collect(Collectors.toList());
        if (categoryDto.getSubscriberIds() != null)
            subscribers = categoryDto.getSubscriberIds().stream().map(e -> userRepository.findById(e).orElse(null)).collect(Collectors.toList());
        return new Category(categoryDto.getName(),categoryDto.getDescription(),articles,subscribers);
    }
}
