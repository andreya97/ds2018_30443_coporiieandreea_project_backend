package ds.project.newsagency.repositories;

import ds.project.newsagency.entities.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleRepository extends JpaRepository<Article, Long> {
    public Article findByTitle(String title);
    public List<Article> findByCategory_Id(Long id);
    public List<Article> findAllByIdIn(List<Long> ids);
}