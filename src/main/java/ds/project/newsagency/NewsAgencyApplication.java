package ds.project.newsagency;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = "http://localhost:4200")
@SpringBootApplication
@ComponentScan(basePackages="ds.project")
public class NewsAgencyApplication {

    public static void main(String[] args) {
        SpringApplication.run(NewsAgencyApplication.class, args);
    }
}
